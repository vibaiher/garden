# Garden

## Requisites

- docker
- docker-compose

## Usage

- Start server: `docker-compose up --build`
- Open: `http://localhost:3000`
